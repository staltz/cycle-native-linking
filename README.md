# Cycle Native Linking

**A Cycle.js Driver for interacting with React Native [Linking](https://facebook.github.io/react-native/docs/0.55/alert)**

```
npm install cycle-native-linking
```

## Usage

### Sink

Stream of URL (strings) to open as links.

### Source

Stream of deep link events (strings) targetted at the current app.

## License

MIT
