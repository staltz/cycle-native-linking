import xs, {Stream, Listener} from 'xstream';
import {adapt} from '@cycle/run/lib/adapt';
import {Linking} from 'react-native';

export function linkingDriver(url$: Stream<string>): Stream<string> {
  url$.addListener({
    next: (url) => {
      Linking.openURL(url).catch((err) => {
        console.error(err);
      });
    },
  });

  return adapt(
    xs
      .merge(
        xs.fromPromise<string | null>(Linking.getInitialURL()),
        xs.create<string>({
          start: function start(this: any, listener: Listener<string>) {
            this.fn = (event: {url: string}) => {
              listener.next(event.url);
            };
            Linking.addEventListener('url', this.fn);
          },

          stop: function stop(this: any) {
            Linking.removeEventListener('url', this.fn);
          },
        }),
      )
      .filter((s: string | null) => !!s),
  );
}
